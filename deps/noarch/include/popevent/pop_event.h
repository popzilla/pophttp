/*
 * Description: pop event对外接口
 * Author: xiasonglee@gmail.com
 * Date: 2023-3-4
 */

#ifndef POP_EVENT_H
#define POP_EVENT_H

#include <stdint.h>

enum {
    POP_EVENT_IN = 0x001,
    POP_EVENT_OUT = 0x002,
    POP_EVENT_HUP = 0x004,
    POP_EVENT_RDHUP = 0x008,
    POP_EVENT_ERR = 0x010,
    POP_EVENT_ET = 1 << 31
};

typedef struct pop_event pop_event_t;

typedef void (*pop_event_cb_t)(pop_event_t *event, uint32_t what, void *usrdata);

int pop_event_init(void);

pop_event_t *pop_event_create(int fd, void *usrdata);

int pop_event_add_watch(pop_event_t *event, uint32_t what, pop_event_cb_t cb);

int pop_event_mod_watch(pop_event_t *event, uint32_t what, pop_event_cb_t cb);

int pop_event_del_watch(pop_event_t *event);

void pop_event_destroy(pop_event_t *event);

void pop_event_loop(void);

void pop_event_fini(void);

#endif

