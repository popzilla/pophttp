/*
 * Description: HTTP客户端入口函数
 * Author: xiasonglee@gmail.com
 * Date: 2023-6-23
 */

#include <unistd.h>
#include <errno.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include "pop_event.h"

#define SERVER_IP    "127.0.0.1"
#define SERVER_PORT  8443
#define RECV_BUF_LEN 512

struct client {
    int input_fd;
    int conn_fd;
    bool server_hup;
};

static void set_fd_nonblock(int fd)
{
    int flags = fcntl(fd, F_GETFL, 0);
    if (flags < 0) {
        return;
    }
    fcntl(fd, F_SETFL, flags | O_NONBLOCK);
}

static int connect_server(uint32_t ip, uint16_t port)
{
    int fd = socket(AF_INET, SOCK_STREAM, 0);
    if (fd < 0) {
        return -1;
    }
    set_fd_nonblock(fd);

    struct sockaddr_in sin = { 0 };
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = htonl(ip);
    sin.sin_port = htons(port);

    int ret = connect(fd, (struct sockaddr*)&sin, sizeof(sin));
    if (ret < 0) {
        if (errno != EINPROGRESS) {
            return -1;
        } else {
            printf("Connect in progress.\n");
        }
    }
    return fd;
}

static void client_read(struct client *client)
{
    uint8_t buf[RECV_BUF_LEN];
    int len = read(client->conn_fd, buf, sizeof(buf) - 1);
    buf[len] = '\0';
    printf("client recv: %s", buf);
}

static void client_write(int fd, uint8_t *buf, size_t len)
{
    write(fd, buf, len);
    printf("client write: %s", buf);
}

static void input_read(struct client *client)
{
    uint8_t buf[RECV_BUF_LEN];
    int len = read(client->input_fd, buf, sizeof(buf) - 1);
    client_write(client->conn_fd, buf, len);
}

static void server_hup(struct client *client)
{
    printf("Server hang up.\n");
    close(client->conn_fd);
    client->conn_fd = -1;
    client->server_hup = true;
}

static void client_proc(pop_event_t *event, uint32_t what, void *usrdata)
{
    struct client *client = (struct client *)usrdata;
    if (what & POP_EVENT_IN) {
        client_read(client);
    }

    if (what & POP_EVENT_RDHUP) {
        server_hup(client);
    }

    if (client->server_hup) {
        pop_event_destroy(event);
    }
}

static void input_proc(pop_event_t *event, uint32_t what, void *usrdata)
{
     struct client *client = (struct client *)usrdata;
    if (what & POP_EVENT_IN) {
        input_read(client);
    }
}

int main(void)
{
    pop_event_init();

    uint32_t ip = ntohl(inet_addr(SERVER_IP));
    uint16_t port = SERVER_PORT;
    int conn_fd = connect_server(ip, port);
    if (conn_fd < 0) {
        pop_event_fini();
        return 1;
    }

    int stdin = 0; /* 标准输入 */

    static struct client client = { 0 };
    client.input_fd = stdin;
    client.conn_fd = conn_fd;

    uint32_t what = POP_EVENT_ET | POP_EVENT_IN | POP_EVENT_RDHUP;
    pop_event_t *conn_evt = pop_event_create(conn_fd, &client);
    pop_event_add_watch(conn_evt, what, client_proc);

    pop_event_t *input_evt = pop_event_create(stdin, &client);
    what = POP_EVENT_ET | POP_EVENT_IN;
    pop_event_add_watch(input_evt, what, input_proc);

    pop_event_loop();
    return 0;
}
