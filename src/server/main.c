/*
 * Description: HTTP服务器入口函数
 * Author: xiasonglee@gmail.com
 * Date: 2023-6-23
 */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include "pop_event.h"

#define SERVER_IP       "0.0.0.0"
#define SERVER_POPRT    8443
#define MAX_BACKLOG_NUM 128

#define RECV_BUF_LEN 512
#define SEND_BUF_LEN 512

static void conn_proc(pop_event_t *event, uint32_t what, void *usrdata);

static void set_fd_nonblock(int fd)
{
    int flags = fcntl(fd, F_GETFL, 0);
    if (flags < 0) {
        return;
    }
    fcntl(fd, F_SETFL, flags | O_NONBLOCK);
}

static void set_fd_reuse(int fd)
{
    int flags = fcntl(fd, F_GETFL, 0);
    if (flags < 0) {
        return;
    }

    flags |= (SO_REUSEPORT | SO_REUSEADDR);
    fcntl(fd, F_SETFL, flags);
}

static int server_listen(uint32_t ip, uint16_t port)
{
    int fd = socket(AF_INET, SOCK_STREAM, 0);
    if (fd < 0) {
        printf("Failed to create socket, errno %d.\n", errno);
        return -1;
    }
    set_fd_reuse(fd);
    set_fd_nonblock(fd);

    struct sockaddr_in sin = { 0 };
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = htonl(ip);
    sin.sin_port = htons(port);
    if (bind(fd, (struct sockaddr*)&sin, sizeof(sin)) < 0) {
        printf("Failed to bind socket, errno %d.\n", errno);
        close(fd);
        return -1;
    }

    if (listen(fd, MAX_BACKLOG_NUM) < 0) {
        printf("Failed to listen, errno %d\n", errno);
        close(fd);
        return -1;
    }
    return fd;
}

struct sock_conn {
    int fd;
    pop_event_t *event;
    uint8_t recv_buf[RECV_BUF_LEN];
    size_t recv_len;
    uint8_t send_buf[SEND_BUF_LEN];
    size_t send_len;
    bool destroy;
};

static void conn_trigger_send(struct sock_conn *conn)
{
    uint32_t what = POP_EVENT_IN | POP_EVENT_RDHUP |
        POP_EVENT_ET | POP_EVENT_OUT;
    pop_event_mod_watch(conn->event, what, conn_proc);
    printf("Conn trigger send %d\n", conn->fd);
}

static void conn_pause_send(struct sock_conn *conn)
{
    uint32_t what = POP_EVENT_IN | POP_EVENT_RDHUP |
        POP_EVENT_ET;
    pop_event_mod_watch(conn->event, what, conn_proc);
    printf("Conn pause send %d\n", conn->fd);
}

static void conn_send_proc(struct sock_conn *conn)
{
    int left = conn->send_len;
    int len;
    do {
        uint8_t *send_pos = conn->send_buf + (conn->send_len - left);
        len = write(conn->fd, send_pos, left);
        if (len == 0) {
            conn->destroy = true;
        } else if (len < 0) {
            break;
        }

        left -= len;
    } while (left > 0);

    if (left <= 0) {
        conn_pause_send(conn);
    }
}

static void conn_recv_proc(struct sock_conn *conn)
{
    int len;
    conn->recv_len = 0;
    do {
        uint8_t recv_buf[RECV_BUF_LEN];
        len = read(conn->fd, recv_buf, sizeof(recv_buf) - 1);
        if (len == 0) {
            printf("Recv error fd %d\n", conn->fd);
            conn->destroy = true;
            return;
        } else if (len < 0) {
            break;
        }

        /* 缓冲区满了先不收了，实际需要业务处理时判断 */
        if (conn->recv_len + len >= sizeof(conn->recv_buf)) {
            continue;
        }

        memcpy(conn->recv_buf + conn->recv_len, recv_buf, len);
        conn->recv_len += len;
   } while(len > 0);

    conn->recv_buf[conn->recv_len] = '\0';
    printf("recv: %s\n", conn->recv_buf);
    memcpy(conn->send_buf, conn->recv_buf, sizeof(conn->recv_buf));
    conn->send_len = conn->recv_len;

    conn_trigger_send(conn);
}

static void conn_hup_proc(struct sock_conn *conn)
{
    printf("Peer closed fd %d\n", conn->fd);
    close(conn->fd);
    conn->fd = -1;
    conn->destroy = true;
}

static void conn_proc(pop_event_t *event, uint32_t what, void *usrdata)
{
    struct sock_conn *conn = (struct sock_conn *)usrdata;
    if (what & POP_EVENT_IN) {
        conn_recv_proc(conn);
    }

    if (what & POP_EVENT_OUT) {
        conn_send_proc(conn);
    }

    if (what & POP_EVENT_RDHUP) {
        conn_hup_proc(conn);
    }

    if (conn->destroy) {
        /* FIXME: conn内存需在循环外释放 */
        pop_event_destroy(event);
    }
}

static void server_proc(pop_event_t *event, uint32_t what, void *usrdata)
{
    int fd = (int)(intptr_t)usrdata;

    struct sockaddr_in sin = { 0 };
    socklen_t sin_len = sizeof(sin);
    int conn_fd = accept(fd, (struct sockaddr *)&sin, &sin_len);
    if (conn_fd < 0) {
        printf("Failed to accept conn fd, errno %d\n", errno);
        return;
    }
    set_fd_nonblock(conn_fd);

    char ip[32] = { 0 }; /* 32字节足够放下IP地址 */
    inet_ntop(AF_INET, &sin.sin_addr, ip, sizeof(ip));
    printf("accept conn socket fd %d addr %s port %hu.\n",
        fd, ip, ntohs(sin.sin_port));

    struct sock_conn *conn = (struct sock_conn *)malloc(sizeof(*conn));
    if (conn == NULL) {
        close(conn_fd);
        return;
    }
    *conn = (struct sock_conn) { 0 };
    conn->fd = conn_fd;
    conn->event = pop_event_create(conn_fd, conn);

    uint32_t conn_what = POP_EVENT_IN | POP_EVENT_RDHUP | POP_EVENT_ET;
    pop_event_add_watch(conn->event, conn_what, conn_proc);
}

int main(void)
{
    pop_event_init();

    uint32_t ip = ntohl(inet_addr(SERVER_IP));
    uint16_t port = SERVER_POPRT;
    int fd = server_listen(ip, port);
    if (fd < 0) {
        pop_event_fini();
        return 1;
    }

    uint32_t what = POP_EVENT_IN | POP_EVENT_ET;
    pop_event_t *evt = pop_event_create(fd, (void *)(intptr_t)fd);
    pop_event_add_watch(evt, what, server_proc);
    pop_event_loop();
    return 0;
}
